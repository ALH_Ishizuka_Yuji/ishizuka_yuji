<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css"><%@include file="/css/normalize.css" %></style>
<style type="text/css"><%@include file="/css/home.css" %></style>
<title>ホーム</title>
</head>
<body>

	<div class="searchHeader">
		<form action="SearchByCategory" method="post">
		<label>検索文字：</label><input name="categoryName" id="categoryName" />
		<input id="submit_button" type="submit" value="カテゴリー検索" />
		</form>
	</div>

	<div class="searchDateHeader">
		<form action="SearchByDate" method="post">
		<label>日付選択：</label>
		<input name="startDate" type="date" id="searchDate"/>~
				<input name="endDate" type="date" id="searchDate"/>
		<input id="submit_button" type="submit" value="日付検索" />
		</form>
	</div>

	<div class="sideMenu">
		<div class="sticky">
			<h3>投稿一覧</h3>
			<div class="userInfo">
				<c:if test="${ not empty loginUser }">
	            	ログイン@<c:out value="${loginUser.loginName}" />
				</c:if>
			</div>
				<form action="Top">
				<input id="stickyButton" type="submit" value="ホーム" />
				</form>
				<c:if test="${loginUser.admin}">
					<form action="RegisterUser">
					<input id="stickyButton" type="submit" value="ユーザー登録" />
					</form>
					<form action="UserManagement">
					<input id="stickyButton" type="submit" value="ユーザー管理" />
					</form>
				</c:if>
				<form action="./AddPost">
				<input id="stickyButton" type="submit" value="新規投稿" />
				</form>
				<form action="Logout">
				<input id="stickyButton" type="submit" value="ログアウト" />
				</form>
		</div>
	</div>

	<div class="mainContents">
		<c:if test="${ not empty validationMessages }">
			<div class="validationArea">
	 			<ul>
				 	<c:forEach items="${validationMessages}" var="message">
			 	 		<li><c:out value="${message}"/>
					</c:forEach>
				</ul>
				<c:remove var="validationMessages" scope="session" />
			</div>
		</c:if>

		<div class="contentsArea">
		    <c:forEach items="${allPosts}" var="post">
		    	<c:if test="${post.userId == loginUser.loginId}">
		    		<div class="deleteButton">
						<form action="DeletePost" method="post">
						<input type="hidden" name="postId" value="${post.id}">
						<input id="submit_button" type="submit" value="投稿削除" /><br>
						</form>
					</div>
				</c:if>
                <div class="postInfo">
	                <div class="titleArea">
	                	<c:out value="${post.title}" />
	                </div>
                    <div class="nameArea">
                    	<span class="name">　投稿者：<c:out value="${post.userName}" /></span>
                    </div>
                    <div class="dateArea">
                    	　日付：<fmt:formatDate value="${post.posted_date}" pattern="yyyy/MM/dd HH:mm:ss" />
                    </div>
                    <div class="categoryArea">
                    	<span class="category">　カテゴリ：<c:out value="${post.category}" /></span>
                    </div>
                    <div class="postArea">
                    	<p><c:out value="${post.text}" /></p>
                    </div>
                    <div class="commentArea">
                    	<c:out value="${post.title}" />へのコメント<br>
                    	<c:forEach items="${allComments}" var="entry">
		            	<c:if test="${entry.key == post.id}">
		            		<c:forEach items="${entry.value}" var="comment">
			            		<div class="eachComment">
			            			<div class="statement">
				            			<p><span class="contents"><c:out value="${comment.comment}" /></span></p>
				            		</div>
				            		<div class="commentInfo">
					            		<span class="commentUser">@<c:out value="${comment.userName}"/>　</span>
					            		<fmt:formatDate value="${comment.commented_date}" pattern="yyyy/MM/dd HH:mm:ss" />
				            		</div>
				            		<div class="commentDelete">
					            		<c:if test="${comment.userId == loginUser.loginId}">
											<form action="DeleteComment" method="post">
											<input type="hidden" name="commentId" value="${comment.id}">
											<input id="submit_button" type="submit" value="コメント削除" /><br><br>
											</form>
										</c:if>
									</div>
								</div>
							</c:forEach>
						</c:if>
	            	</c:forEach>
                    </div>
                    <div class="inputArea">
                    	<form action="AddComment" method="post">
						<textarea name="comment"cols="40" rows="7" placeholder="コメントを入力してください"></textarea><br>
						<input type="hidden" name="userId" value="${loginUser.loginId}">
						<input type="hidden" name="postId" value="${post.id}">
						<input id="submit_button" type="submit" value="コメント" />
						</form>
                    </div>
                </div>
	            <br><br>
		    </c:forEach>
	    	</div>
		</div>

	<footer><address> Copyright(C)2019 Yuji Ishizuka, Allright Reserved.</address></footer>
</body>
</html>