<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css"><%@include file="/css/normalize.css" %></style>
<style type="text/css"><%@include file="/css/login.css" %></style>
<title>ログイン画面</title>
</head>
<body>
	<div class="mainContents">
	<div class="center">
		<h1 class="loginHeading">ログイン</h1>
	</div>

		<div class="errorMessageArea">
			<c:if test="${ not empty message }">
			        <ul>
			        	<li><c:out value="${message}" />
			        </ul>
			    <c:remove var="message" scope="session"/>
			</c:if>
		</div>

		<div class="LoginArea">
			<form action="Login" method="post"><br />
			    <p><label for="id">ユーザーID: </label><input name="id" id="id"/></p>
			    <p><label for="password">パスワード: </label><input name="password" type="password" id="password"/></p>
			    <p><input id="submit_button" type="submit" value="ログイン" /></p>
			</form>
		</div>

	</div>
	<footer><address> Copyright(C)2019 Yuji Ishizuka, Allright Reserved.</address></footer>
</body>
</html>