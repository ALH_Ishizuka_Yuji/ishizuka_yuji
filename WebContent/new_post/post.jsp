<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css"><%@include file="/css/normalize.css" %></style>
<style type="text/css"><%@include file="/css/Post.css" %></style>
<title>新規投稿</title>
</head>
<body>
	<div class="sideMenu">
		<div class="sticky">
			<h3>新規投稿</h3>
			<div class="userInfo">
				<c:if test="${ not empty loginUser }">
				    <div class="profile">
				        <div class="account">
				            ログイン@<c:out value="${loginUser.loginName}" />
				        </div>
				    </div>
				</c:if>
			</div>
				<form action="Top">
				<input id="stickyButton" type="submit" value="ホーム" />
				</form>
				<c:if test="${loginUser.admin}">
					<form action="RegisterUser">
					<input id="stickyButton" type="submit" value="ユーザー登録" />
					</form>
					<form action="UserManagement">
					<input id="stickyButton" type="submit" value="ユーザー管理" />
					</form>
				</c:if>
				<form action="./AddPost">
				<input id="stickyButton" type="submit" value="新規投稿" />
				</form>
				<form action="Logout">
				<input id="stickyButton" type="submit" value="ログアウト" />
				</form>
		</div>
	</div>

	<div class="mainContents">
		<h1>投稿内容</h1><br />
		<c:if test="${ not empty validationMessages }">
	 		<div class="validationArea">
	 			<ul>
				 	<c:forEach items="${validationMessages}" var="message">
			 	 		<li><c:out value="${message}"/>
					</c:forEach>
				</ul>
			</div>
			<c:remove var="validationMessages" scope="session" />
		</c:if>

		<div class="postArea">
	        <form action="AddPost" method="post">
	            <label for="title">件名：</label> <input name="title" id="title" placeholder="（30文字まで）"/><br />
	            <label for="category">カテゴリ：</label> <input name="category" id="category" placeholder="（10文字まで）"/><br />
	            <label for="contents">本文：</label> <textarea name="contents" id="contents" cols="100" rows="15" placeholder="内容を入力してください（1000文字まで)"></textarea><br />
	            <label for="submit_button"></label><input id="submit_button" type="submit" value="投稿">
	       </form><br />
		</div>
	</div>

	<footer><address> Copyright(C)2019 Yuji Ishizuka, Allright Reserved.</address></footer>
</body>
</html>