<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css"><%@include file="/css/normalize.css" %></style>
<style type="text/css"><%@include file="/css/RegisterUser.css" %></style>
<title>ユーザー登録</title>
</head>
<body>
	<div class="sideMenu">
		<div class="sticky">
			<h3>ユーザー登録</h3>
			<div class="userInfo">
				<c:if test="${ not empty loginUser }">
				    <div class="profile">
				        <div class="account">
				            ログイン@<c:out value="${loginUser.loginName}" />
				        </div>
				    </div>
				</c:if>
			</div>
				<form action="Top">
				<input id="stickyButton" type="submit" value="ホーム" />
				</form>
				<c:if test="${loginUser.admin}">
					<form action="RegisterUser">
					<input id="stickyButton" type="submit" value="ユーザー登録" />
					</form>
					<form action="UserManagement">
					<input id="stickyButton" type="submit" value="ユーザー管理" />
					</form>
				</c:if>
				<form action="./AddPost">
				<input id="stickyButton" type="submit" value="新規投稿" />
				</form>
				<form action="Logout">
				<input id="stickyButton" type="submit" value="ログアウト" />
				</form>
		</div>
	</div>

	<div class="mainContents">
	<h1>登録内容</h1>
		<c:if test="${ not empty validationMessages }">
		    <div class="validationArea">
		         <ul>
		             <c:forEach items="${validationMessages}" var="message">
		                 <li><c:out value="${message}" />
		             </c:forEach>
		         </ul>
     		</div>
     		<c:remove var="errorMessages" scope="session" />
	 	</c:if>
	<div class="registerArea">
		<form action="RegisterUser" method="post">
		    <label for="id">ユーザーID:</label><input name="id" id="id" /><br />
		    <label for="password">パスワード:</label><input name="password" type="password" id="password" /> <br />
		    <label for="passwordConfirm">パスワード確認:</label><input name="passwordConfirm" type="password" id="passwordConfirm" /> <br />
		    <label for="name">名称:</label><input name="name" id="name" /> <br />
		    <label for="branch">支店:</label>
		    <select name="branch">
		    	<option value="0">支店を選択してください</option>
		    	<option value="10001">本社</option>
				<option value="10002">支店A</option>
				<option value="10003">支店B</option>
				<option value="10004">支店C</option>
		    </select> <br />
		    <label for="position">部署・役職:</label>
		    <select name="position">
		    	<option value="4">部署・役職を選択してください</option>
		    	<option value="0">情報管理担当者</option>
				<option value="1">総務人事担当者</option>
				<option value="2">支店長</option>
				<option value="3">社員</option>
		    </select> <br />
			<label for="user">アカウント状態:</label>
			<select name="user">
				<option value="1" selected>始動</option>
				<option value="0">停止</option>
			</select><br />
			<label for="submit_button"></label><input id="submit_button" type="submit" value="登録" /> <br />
		 </form>
		 <br />
	 </div>
	</div>

	<footer><address> Copyright(C)2019 Yuji Ishizuka, Allright Reserved.</address></footer>
</body>
</html>