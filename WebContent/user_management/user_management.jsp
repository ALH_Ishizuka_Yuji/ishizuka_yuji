<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="beans.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css"><%@include file="/css/normalize.css" %></style>
<style type="text/css"><%@include file="/css/userManagement.css" %></style>
<title>ユーザー管理</title>
</head>
<body>
	<div class="sideMenu">
		<div class="sticky">
			<h3>ユーザー管理</h3>
			<div class="userInfo">
				<c:if test="${ not empty loginUser }">
				    <div class="profile">
				        <div class="account">
				            ログイン@<c:out value="${loginUser.loginName}" />
				        </div>
				    </div>
				</c:if>
			</div>
				<form action="Top">
				<input id="stickyButton" type="submit" value="ホーム" />
				</form>
				<c:if test="${loginUser.admin}">
					<form action="RegisterUser">
					<input id="stickyButton" type="submit" value="ユーザー登録" />
					</form>
					<form action="UserManagement">
					<input id="stickyButton" type="submit" value="ユーザー管理" />
					</form>
				</c:if>
				<form action="./AddPost">
				<input id="stickyButton" type="submit" value="新規投稿" />
				</form>
				<form action="Logout">
				<input id="stickyButton" type="submit" value="ログアウト" />
				</form>
		</div>
	</div>

	<div class="mainContents">
		<c:if test="${!empty message }">
		${ message }
		</c:if>

		<div class="contentsArea">
			<table border="1">
			<caption>ユーザーリスト</caption>
				<tr>
				<th>ユーザーID</th>
				<th>名称</th>
				<th>支店</th>
				<th>部署・役職</th>
				<th>アカウント状態</th>
				<th>編集</th>
				<th></th>
				</tr>
				<c:forEach items="${userList}" var="user">
					<tr>
					<td><c:out value="${user.id}" /></td>
					<td><c:out value="${user.name}" /></td>
					<td><c:out value="${user.branchName}" /></td>
					<td><c:out value="${user.positionName}" /></td>
					<td><c:out value="${user.userCondition}" /></td>
					<td>
						<form action="EditUser" method="post">
						<input type="hidden" name="userId" value="${user.id}">
						<input id="submit_button" type="submit" value="ユーザー編集" /></form>
					</td>
					<td>
					<c:choose>
						<c:when test="${loginUser.loginId != user.id}">
							<form action="ChangeUserCondition" method="post" onSubmit="return check()">
							<input type="hidden" name="userId" value="${user.id}">
							<input id="submit_button" type="submit" value="状態変更" /></form>
						</c:when>
						<c:otherwise>
							ログイン中
						</c:otherwise>
					</c:choose>
					</td>
					</tr>
				</c:forEach>
			</table>
			<form action="RegisterUser"><br />
			<input id="submit_button" type="submit" value="新規ユーザー登録" /> <br />
			</form>
		</div>


	</div>

	<footer><address> Copyright(C)2019 Yuji Ishizuka, Allright Reserved.</address></footer>

	<script type="text/javascript">
		function check(){
			if(window.confirm('アカウント状態を変更しますか？')){ // 確認ダイアログを表示
				return true; // 「OK」時は送信を実行
			}
			else{ // 「キャンセル」時の処理
				window.alert('キャンセルされました'); // 警告ダイアログを表示
				return false; // 送信を中止
			}
		}
	</script>
</body>
</html>