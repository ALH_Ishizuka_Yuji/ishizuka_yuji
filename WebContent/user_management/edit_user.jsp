<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="beans.User"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css"><%@include file="/css/normalize.css" %></style>
<style type="text/css"><%@include file="/css/EditUser.css" %></style>
<title>ユーザー編集</title>
</head>
<body>
	<div class="sideMenu">
		<div class="sticky">
			<h3>ユーザー編集</h3>
			<div class="userInfo">
				<c:if test="${ not empty loginUser }">
				    <div class="profile">
				        <div class="account">
				            ログイン@<c:out value="${loginUser.loginName}" />
				        </div>
				    </div>
				</c:if>
			</div>
				<form action="Top">
				<input id="stickyButton" type="submit" value="ホーム" />
				</form>
				<c:if test="${loginUser.admin}">
					<form action="RegisterUser">
					<input id="stickyButton" type="submit" value="ユーザー登録" />
					</form>
					<form action="UserManagement">
					<input id="stickyButton" type="submit" value="ユーザー管理" />
					</form>
				</c:if>
				<form action="./AddPost">
				<input id="stickyButton" type="submit" value="新規投稿" />
				</form>
				<form action="Logout">
				<input id="stickyButton" type="submit" value="ログアウト" />
				</form>
		</div>
	</div>

	<c:choose>
		<c:when test="${empty editingData}">
			<c:set var="editingData" value="${user}" scope="session"></c:set>
			<c:set var="previousId" value="${user.id}" scope="session"></c:set>
			<c:set var="formData" value="${user}"></c:set>
		</c:when>
		<c:otherwise>
			<c:set var="formData" value="${editingData}"></c:set>
		</c:otherwise>
	</c:choose>

	<div class="mainContents">
	<h1>編集内容</h1>
		 <c:if test="${ not empty validationMessages }">
		     <div class="validationArea">
		         <ul>
		             <c:forEach items="${validationMessages}" var="message">
		                 <li><c:out value="${message}" />
		             </c:forEach>
		         </ul>
		     </div>
		     <c:remove var="errorMessages" scope="session" />
		 </c:if>

		<div class="editArea">
		 <form action="ChangeUser" method="post">
		    <br /><label for="id">ユーザーID:</label> <input name="id" id="id" value="${formData.id}" /> <br />
		    <label for="password">パスワード:</label> <input name="password" type="password" id="password" /> <br />
		    <label for="passwordConfirm">パスワード確認:</label> <input name="passwordConfirm" type="password" id="passwordConfirm" /> <br />
		    <label for="name">名称:</label> <input name="name" id="name" value="${formData.name}" /> <br />
		    <label for="branch">支店:</label>
		    <select name="branch">
			    <c:forEach items="${branchMap}" var="branchEntry">
			    	<c:choose>
			    		<c:when test="${branchEntry.key == formData.branch}">
			    			<option value="${branchEntry.key}" selected><c:out value="${branchEntry.value}"/></option>
			    		</c:when>
			    		<c:otherwise>
							<option value="${branchEntry.key}"><c:out value="${branchEntry.value}"/></option>
						</c:otherwise>
			    	</c:choose>
		    	</c:forEach>
		    </select> <br />
			<label for="position">部署・役職:</label>
			<select name="position">
			    <c:forEach items="${positionMap}" var="positionEntry">
			    	<c:choose>
			    		<c:when test="${positionEntry.key == formData.position}">
			    			<option value="${positionEntry.key}" selected><c:out value="${positionEntry.value}"/></option>
			    		</c:when>
			    		<c:otherwise>
							<option value="${positionEntry.key}"><c:out value="${positionEntry.value}"/></option>
						</c:otherwise>
			    	</c:choose>
		    	</c:forEach>
		    </select> <br />
			<c:set var="num" value="${formData.user}"/>
			<c:if test="${ num == 1}">
				<label for="active">アカウント状態:</label>
				<select name="active"> <option value="1" selected >始動</option> <option value="0" >停止</option>
				</select> <br />
			</c:if>
			<c:if test="${ num != 1}">
				<label for="active">アカウント状態:</label>
				<select name="active"> <option value="1" >始動</option> <option value="0" selected>停止</option>
				</select> <br />
			</c:if>
		    <br />
		    <input type="hidden" name="previousUserId" value="${previousId}">
		    <label for="submit_button"></label><input id="submit_button" type="submit" value="変更" /> <br />
		 </form>
		 <br />
		 </div>
	</div>

	<footer><address> Copyright(C)2019 Yuji Ishizuka, Allright Reserved.</address></footer>
</body>
</html>