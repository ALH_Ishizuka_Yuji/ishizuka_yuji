package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import beans.LoginUser;
import beans.User;
import utils.DBUtil;

public class UserDao{
	static Connection connection = null;
	static PreparedStatement statement = null;

	public static LoginUser checkUser(String id, String password) {
		LoginUser loginUser = null;
		int userCondition = 0;
		try {
			connection = DBUtil.getConnection();
	        statement = connection.prepareStatement("SELECT * FROM users WHERE id = ? AND password = ?");
	        statement.setString(1, id);
	        statement.setString(2, password);
	        ResultSet rs = statement.executeQuery();
	        while(rs.next()) {
				String resultId = rs.getString("id");
				String resultPass = rs.getString("password");
				userCondition = Integer.parseInt(rs.getString("active"));
				if (resultId.equals(id) && resultPass.equals(password)) {
					loginUser = new LoginUser();
					loginUser.setLoginId(resultId);
					loginUser.setLoginName(rs.getString("name"));
					loginUser.setAdmin(isEditer(loginUser));
				}
				else {
					loginUser = null;
				}
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		if (loginUser != null && userCondition == 1) {
			return loginUser;
		} else {
			return null;
		}
	}

	public static User findUser(String id) {
		User user = new User();
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT * FROM users WHERE id = ?");
	        statement.setString(1, id);
	        ResultSet rs = statement.executeQuery();
	        rs.next();
	        user.setId(id);
	        user.setPassword(rs.getString("password"));
	        user.setName(rs.getString("name"));
	        user.setBranch(Integer.parseInt(rs.getString("branch_id")));
	        user.setPosition(Integer.parseInt(rs.getString("position_id")));
	        user.setUser(Integer.parseInt(rs.getString("active")));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		return user;
	}

	public static List<User> getAllUser() {
		List<User> userList = new ArrayList<User>();
		try {
			User user = null;
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT users.id, users.name AS user_name, "
					+ "branches.name AS branch_name, positions.name AS position_name, users.active "
					+ "FROM users INNER JOIN branches ON users.branch_id = branches.id "
					+ "INNER JOIN positions ON users.position_id = positions.id;");
			ResultSet rs = statement.executeQuery();
			while(rs.next()) {
				user = new User();
				user.setId(rs.getString("id"));
				user.setName(rs.getString("user_name"));
				user.setBranchName(rs.getString("branch_name"));
				user.setPositionName(rs.getString("position_name"));
				user.setUser(Integer.parseInt(rs.getString("active")));
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		return userList;
	}

	public static void changeUserCondition(String id) {
		try {
			String condition = null;
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT active FROM users WHERE id = ?");
			statement.setString(1, id);
			ResultSet rs = statement.executeQuery();
			rs.next();
			condition = rs.getString("active");
			if (condition.equals("1")) {
				statement = connection.prepareStatement("UPDATE users SET active = 0 WHERE id = ?");
				statement.setString(1, id);
			} else {
				statement = connection.prepareStatement("UPDATE users SET active = 1 WHERE id = ?");
				statement.setString(1, id);
			}
	        int updateCount = statement.executeUpdate();
	        if (updateCount == 1) {
	            System.out.println("変更成功");
	            connection.commit();
	        } else {
	            System.out.println("変更失敗");
	            connection.rollback();
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
	}

	public static void insert(User user) {
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("INSERT INTO users (id, password, name, branch_id, "
					+ "position_id, active) VALUES (?, ?, ?, ?, ?, ?)");
			statement.setString(1, user.getId());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getName());
			statement.setString(4, Integer.toString(user.getBranch()));
			statement.setString(5, Integer.toString(user.getPosition()));
			statement.setString(6, Integer.toString(user.getUser()));
			int updateCount = statement.executeUpdate();
	        if (updateCount == 1) {
	            System.out.println("登録完了");
	            connection.commit();
	        } else {
	            System.out.println("登録失敗");
	            connection.rollback();
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
	}

	public static void edit(User user, String previousUserId) {
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("UPDATE users SET id = ?, password = ?, name = ?, "
					+ "branch_id = ?, position_id = ?, active = ? WHERE id = ?");
			statement.setString(1, user.getId());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getName());
			statement.setString(4, Integer.toString(user.getBranch()));
			statement.setString(5, Integer.toString(user.getPosition()));
			statement.setString(6, Integer.toString(user.getUser()));
			statement.setString(7, previousUserId);
			int updateCount = statement.executeUpdate();
	        if (updateCount == 1) {
	            System.out.println("編集完了");
	            connection.commit();
	        } else {
	            System.out.println("編集失敗");
	            connection.rollback();
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
	}

	public static Boolean isEditer(LoginUser user) {
		boolean result = false;
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT position_id FROM users WHERE id = ?");
			statement.setString(1, user.getLoginId());
			ResultSet rs = statement.executeQuery();
	        rs.next();
	        if (rs.getInt("position_id") == 1) {
	        	result = true;
	        } else {
	        	result = false;
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		return result;
	}

	public static Map<Integer, String> getAllbranches(){
		Map<Integer, String> branchMap = new LinkedHashMap<Integer, String>();
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT * FROM branches");
			ResultSet rs = statement.executeQuery();
			while(rs.next()) {
				branchMap.put(rs.getInt(1), rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		return branchMap;
	}

	public static Map<Integer, String> getAllPositions() {
		Map<Integer, String> positionMap = new LinkedHashMap<Integer, String>();
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT * FROM positions");
			ResultSet rs = statement.executeQuery();
			while(rs.next()) {
				positionMap.put(rs.getInt(1), rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		return positionMap;
	}
}