package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import beans.UserComment;
import utils.DBUtil;

public class CommentDao {
	static Connection connection = null;
	static PreparedStatement statement = null;

	//コメント追加メソッド
	public static void insert(HttpServletRequest request) {
		connection = DBUtil.getConnection();
		try {
			statement = connection.prepareStatement("INSERT INTO comments (comment, commented_date, user_id, post_id) "
					+"VALUES (?, CURRENT_TIMESTAMP, ?, ?)");
			statement.setString(1, request.getParameter("comment"));
			statement.setString(2, request.getParameter("userId"));
			statement.setString(3, request.getParameter("postId"));
			int updateCount = statement.executeUpdate();
	        if (updateCount == 1) {
	            System.out.println("コメント投稿完了");
	            connection.commit();
	        } else {
	            System.out.println("コメント投稿失敗");
	            connection.rollback();
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
	}

	//コメント削除メソッド
	public static void delete(String commentId) {
		connection = DBUtil.getConnection();
		try {
			statement = connection.prepareStatement("DELETE FROM comments WHERE id = ?");
			statement.setString(1, commentId);
			int deleteCount = statement.executeUpdate();
			if (deleteCount == 1) {
	            System.out.println("コメント削除完了");
	            connection.commit();
	        } else {
	            System.out.println("コメント削除失敗");
	            connection.rollback();
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
	}

	public static void deleteByPostId(String postId) {
		connection = DBUtil.getConnection();
		try {
			statement = connection.prepareStatement("SELECT COUNT(post_id) FROM comments WHERE post_id = ?");
			statement.setString(1, postId);
			ResultSet rs = statement.executeQuery();
			rs.next();
			if (rs.getInt("COUNT(post_id)") == 0) {
				DBUtil.closeConnection(statement, connection);
				return;
			}
			statement = connection.prepareStatement("DELETE FROM comments WHERE post_id = ?");
			statement.setString(1, postId);
			int deleteCount = statement.executeUpdate();
			if (deleteCount == rs.getInt("COUNT(post_id)")) {
	            System.out.println("コメント削除完了");
	            connection.commit();
	        } else {
	            System.out.println("コメント削除失敗");
	            connection.rollback();
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
	}

	public static Map<Integer, List<UserComment>> getAllComments() {
		connection = DBUtil.getConnection();
		Map<Integer, List<UserComment>> commentMap = new  HashMap<Integer, List<UserComment>>();
		List<UserComment> contentsList = null;
		UserComment comment = null;
		try {
			statement = connection.prepareStatement("SELECT comments.id, comments.comment, comments.commented_date, "
					+ "comments.user_id, comments.post_id, users.name "
					+ "FROM comments INNER JOIN users ON comments.user_id = users.id");
			ResultSet rs = statement.executeQuery();
			while(rs.next()) {
				contentsList = new ArrayList<UserComment>();
				comment = new UserComment();
				comment.setId(rs.getInt("id"));
				comment.setComment(rs.getString("comment"));
				comment.setCommented_date(rs.getTimestamp("commented_date"));
				comment.setUserId(rs.getString("user_id"));
				comment.setPostId(rs.getInt("post_id"));
				comment.setUserName(rs.getString("name"));
				//commentMapにコメントが存在するならvalueであるcommentListにコメントを追加
				if (commentMap.containsKey(comment.getPostId())) {
					commentMap.get(comment.getPostId()).add(comment);
				} else {
					//コメントが存在しないなら新たにcommentMapに追加する
					contentsList.add(comment);
					commentMap.put(comment.getPostId(), contentsList);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		return commentMap;
	}
}
