package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import beans.LoginUser;
import beans.UserMessage;
import utils.DBUtil;

public class PostDao {
	static Connection connection = null;
	static PreparedStatement statement = null;
	//投稿を追加するメソッド
	public static void insert(HttpServletRequest request, LoginUser loginUser) {
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("INSERT INTO posts (subject, content, category, posted_date, user_id) "
					+"VALUES (?, ?, ?, CURRENT_TIMESTAMP, ?)");
			statement.setString(1, request.getParameter("title"));
			statement.setString(2, request.getParameter("contents"));
			statement.setString(3, request.getParameter("category"));
			statement.setString(4, loginUser.getLoginId());
			int updateCount = statement.executeUpdate();
	        if (updateCount == 1) {
	            System.out.println("投稿完了");
	            connection.commit();
	        } else {
	            System.out.println("投稿失敗");
	            connection.rollback();
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
	}

	//全投稿データを取得するメソッド
	public static List<UserMessage> getAllPost() {
		List<UserMessage> allMessages = new ArrayList<UserMessage>();
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT posts.id, posts.subject, posts.content, posts.category, "
					+ "posts.posted_date, posts.user_id, users.name FROM posts INNER JOIN users ON posts.user_id = users.id "
					+"ORDER BY posts.posted_date DESC LIMIT 100");
			ResultSet rs = statement.executeQuery();
			//テーブルからの検索結果をリストに格納
			while (rs.next()) {
				UserMessage message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setTitle(rs.getString("subject"));
				message.setText(rs.getString("content"));
				message.setCategory(rs.getString("category"));
				message.setPosted_date(rs.getTimestamp("posted_date"));
				message.setUserId(rs.getString("user_id"));
				message.setUserName(rs.getString("name"));
				allMessages.add(message);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			DBUtil.closeConnection(statement, connection);
		}
		return allMessages;
	}

	public static void delete(String postId) {
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("DELETE FROM posts WHERE id = ?");
			statement.setString(1, postId);
			int deleteCount = statement.executeUpdate();
			if (deleteCount == 1) {
	            System.out.println("投稿削除完了");
	            connection.commit();
	        } else {
	            System.out.println("投稿削除失敗");
	            System.out.println("postdao");
	            connection.rollback();
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
	}

	public static  List<UserMessage> searchPostByCategory(String word){
		List<UserMessage> postList = new ArrayList<UserMessage>();
		String sqlWord;
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT posts.id, posts.subject, posts.content, posts.category, "
					+ "posts.posted_date, posts.user_id, users.name FROM posts INNER JOIN users ON posts.user_id = users.id "
					+ "WHERE posts.category LIKE ?");
			sqlWord = "%"+word+"%";
			statement.setString(1, sqlWord);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				UserMessage message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setTitle(rs.getString("subject"));
				message.setText(rs.getString("content"));
				message.setCategory(rs.getString("category"));
				message.setPosted_date(rs.getTimestamp("posted_date"));
				message.setUserId(rs.getString("user_id"));
				message.setUserName(rs.getString("name"));
				postList.add(message);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		return postList;
	}

	public static  List<UserMessage> searchPostsByDate(String date1, String date2){
		List<UserMessage> postList = new ArrayList<UserMessage>();
		UserMessage message = null;
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT posts.id, posts.subject, posts.content, posts.category, "
					+ "posts.posted_date, posts.user_id, users.name "
					+ "FROM posts INNER JOIN users ON posts.user_id = users.id "
					+ "WHERE posted_date BETWEEN ? AND (? + INTERVAL 1 DAY)");
			statement.setString(1, date1);
			statement.setString(2, date2);
			ResultSet rs = statement.executeQuery();
			int i=0;
			while (rs.next()) {
				System.out.println((i++));
				message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setTitle(rs.getString("subject"));
				message.setText(rs.getString("content"));
				message.setCategory(rs.getString("category"));
				message.setPosted_date(rs.getTimestamp("posted_date"));
				message.setUserId(rs.getString("user_id"));
				message.setUserName(rs.getString("name"));
				postList.add(message);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		return postList;
	}
}
