package utils;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import beans.UserComment;
import beans.UserMessage;

public class ValidateContent {
	public static List<String> validatePost (HttpServletRequest request){
		List<String> errorMessages = new ArrayList<String>();
		UserMessage post = new UserMessage();
		post.setTitle(request.getParameter("title"));
		post.setText(request.getParameter("contents"));
		post.setCategory(request.getParameter("category"));
		int lineAmount = checkLineAmount(post.getText());

		if (post.getTitle().isEmpty() || post.getTitle().length() > 30) {
			errorMessages.add("件名は30字以下です");
		}
		if (post.getCategory().isEmpty() || post.getTitle().length() > 10) {
			errorMessages.add("カテゴリは10字以下です");
		}
		if (post.getText().isEmpty() || post.getText().length() > 1000+lineAmount) {
			errorMessages.add("本文は1000字以下です");
		}
		return errorMessages;
	}

	public static List<String> validateComment (HttpServletRequest request){
		List<String> errorMessages = new ArrayList<String>();
		UserComment comment = new UserComment();
		comment.setComment(request.getParameter("comment"));
		comment.setPostId(Integer.parseInt(request.getParameter("postId")));
		int lineAmount = checkLineAmount(comment.getComment());

		if (comment.getComment().isEmpty() || comment.getComment().length() > 500+lineAmount) {
			errorMessages.add("コメントは500字以内です");
		}
		if (comment.getPostId() == 0) {
			errorMessages.add("どの投稿にコメントするか選択してください");
		}
		return errorMessages;
	}

	private static int checkLineAmount(String text) {
		String br = System.getProperty("line.separator");
		int start, last;
		int count = 0;
		while(true) {
			start = text.indexOf(br);
			last = text.lastIndexOf(br);
			if(start < 0 && last < 0) {
				break;
			} else if (start == last) {
				count += 2;
				break;
			} else {
				text = text.substring(start+2, last);
				count += 4;
			}
		}
		return count;
	}
}
