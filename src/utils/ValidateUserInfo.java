package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import beans.User;

public class ValidateUserInfo {
	public static List<String> validateRegister(HttpServletRequest request){
		List<String> message = new ArrayList<String>();
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String branch = request.getParameter("branch");
		String position = request.getParameter("position");
		if(id == null || id.isEmpty()) {
			message.add("ユーザーIDは必須入力項目です");
		}
		if (id.length() < 6 || id.length() > 20) {
			message.add("ユーザーIDは6〜20字以内です");
		}
		if (!id.matches("^[\\w]+$")) {
			message.add("ユーザーIDは半角英数字で入力してください");
		}
		if (!password.isEmpty() && (password.length() < 6 || password.length() > 20)) {
			message.add("パスワードは6〜20字以内です");
		}
		if (!password.matches("^[-@+*;:#$%&\\w]+$")) {
			message.add("パスワードは半角英数記号で入力してください");
		}
		if (name.length() > 10 || name == null || name.isEmpty()) {
			message.add("名称は10字以内で入力してください");
		}
		if (branch.length() != 5) {
			message.add("支店を選択してください");
		}
		if (position.equals("4")) {
			message.add("部署・役職を選択してください");
		}
		if (branch.equals("10001") && !(position.equals("0") || position.equals("1"))) {
			message.add("本社にある部署を選択してください");
		}
		if (!branch.equals("10001") && (position.equals("0") || position.equals("1"))) {
			message.add("支店にある役職を選択してください");
		}
		return message;
	}

	public static List<String> validateEdit(HttpServletRequest request){
		List<String> message = new ArrayList<String>();
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String branch = request.getParameter("branch");
		String position = request.getParameter("position");
		if(id == null || id.isEmpty()) {
			message.add("ユーザーIDは必須入力項目です");
		}
		if (id.length() < 6 || id.length() > 20) {
			message.add("ユーザーIDは6〜20字以内です");
		}
		if (!id.matches("^[\\w]+$")) {
			message.add("ユーザーIDは半角英数字で入力してください");
		}
		if (name.length() > 10 || name == null || name.isEmpty()) {
			message.add("名称は10字以内で入力してください");
		}
		if (branch.equals("10001") && !(position.equals("0") || position.equals("1"))) {
			message.add("本社にある部署を選択してください");
		}
		if (!branch.equals("10001") && (position.equals("0") || position.equals("1"))) {
			message.add("支店にある役職を選択してください");
		}
		return message;
	}

	public static Map<String, List<String>> useValidation(HttpServletRequest request) {
		User user = new User();
		user.setId(request.getParameter("id"));
		user.setName(request.getParameter("name"));
		user.setPassword(request.getParameter("password"));
		user.setBranch(Integer.parseInt(request.getParameter("branch")));
		user.setPosition(Integer.parseInt(request.getParameter("position")));
		user.setUser(Integer.parseInt(request.getParameter("user")));

		ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
		Validator validator = vf.getValidator();
		Set<ConstraintViolation<User>> validationResult = validator.validate(user);

		Map<String, List<String>> ret = new HashMap<String, List<String>>();
        for (ConstraintViolation<User> violation : validationResult) {
            String propertyPath = violation.getPropertyPath().toString();
            List<String> messages = ret.get(propertyPath);
            if(messages ==null) {
                messages = new ArrayList<String>();
                ret.put(propertyPath, messages);
            }
            messages.add(violation.getMessage());
        }
		return ret;
	}
}
