package service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import beans.UserComment;
import dao.CommentDao;

public class CommentService {

	public static void addComment(HttpServletRequest request) {
		CommentDao.insert(request);
	}

	public static void deleteComment(HttpServletRequest request) {
		CommentDao.delete(request.getParameter("commentId"));
	}

	public static Map<Integer, List<UserComment>> getAllComments() {
		return CommentDao.getAllComments();
	}
}
