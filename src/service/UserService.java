package service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import beans.LoginUser;
import beans.User;
import dao.UserDao;
import utils.CipherUtil;
import utils.ValidateUserInfo;


public class UserService {
	//ユーザー登録
	public static List<String> signUp(HttpServletRequest request) {
		//validationの使用
		List<String> validationMessages = ValidateUserInfo.validateRegister(request);
		String password1, password2;
		boolean passCheck;

		//各入力値にエラーがない場合にユーザー登録
		if(validationMessages.isEmpty()) {
			password1 = request.getParameter("password");
    		password2 = request.getParameter("passwordConfirm");
    		passCheck = password1.equals(password2);
    		//パスワードと確認パスワードが一致している場合にユーザー登録
    		if(passCheck) {
				User user = new User();
				user.setId(request.getParameter("id"));
				user.setPassword(CipherUtil.encrypt(request.getParameter("password")));
				user.setName(request.getParameter("name"));
				user.setBranch(Integer.parseInt(request.getParameter("branch")));
				user.setPosition(Integer.parseInt(request.getParameter("position")));
				user.setUser(1);
				UserDao.insert(user);
    		} else  {
    			validationMessages.add("パスワードと確認パスワードが一致しません");
    		}
		}
		return validationMessages;
	}

	//ユーザーが管理者権限を持っているか確認
	public static boolean checkEditer(LoginUser loginUser) {
		return UserDao.isEditer(loginUser);
	}

	public static List<String> editUser(HttpServletRequest request) {
		//validationの使用
		User user = null;
		LoginUser loginUser = new LoginUser();
		List<String> validationMessages = null;
		String previousUserId = request.getParameter("previousUserId");
		String password1 = request.getParameter("password");
		String password2 = request.getParameter("passwordConfirm");
		String noChangePassword;

		//パスワードと確認パスワードが入力されていない場合はパスワード以外のユーザー情報の変更
		if (password1.isEmpty() && password2.isEmpty()) {
			user = (User) request.getSession().getAttribute("editingData");
			noChangePassword = user.getPassword();
			validationMessages = ValidateUserInfo.validateEdit(request);
			if (validationMessages.isEmpty()) {
				user = new User();
				user.setId(request.getParameter("id"));
				user.setPassword(noChangePassword);
				user.setName(request.getParameter("name"));
				user.setBranch(Integer.parseInt(request.getParameter("branch")));
				user.setPosition(Integer.parseInt(request.getParameter("position")));
				user.setUser(Integer.parseInt(request.getParameter("active")));
				UserDao.edit(user, previousUserId);
			}
			//パスワードと確認パスワードが入力されている場合はユーザーの全情報の変更
		} else {
			validationMessages = ValidateUserInfo.validateEdit(request);
			boolean passCheck = password1.equals(password2);
			if(passCheck && validationMessages.isEmpty()) {
				user = new User();
				user.setId(request.getParameter("id"));
				user.setPassword(CipherUtil.encrypt(request.getParameter("password")));
				user.setName(request.getParameter("name"));
				user.setBranch(Integer.parseInt(request.getParameter("branch")));
				user.setPosition(Integer.parseInt(request.getParameter("position")));
				user.setUser(Integer.parseInt(request.getParameter("active")));
				UserDao.edit(user, previousUserId);
			} else {
				if(!passCheck) {
					validationMessages.add("パスワードと確認パスワードが一致しません");
				}
			}
		}
		if(user.getUser() == 1) {
			loginUser.setAdmin(true);
		} else {
			loginUser.setAdmin(false);
		}
		loginUser.setLoginId(user.getId());
		loginUser.setLoginId(user.getName());
		request.getSession().setAttribute("loginUser", loginUser);
		return validationMessages;
	}

	public static LoginUser changeLoginUser(LoginUser preUser, User user) {

		return null;
	}

	//全ユーザー情報の取得
	public static List<User> getAllUser(){
		return UserDao.getAllUser();
	}

	public static void changeUserCondition(String userId) {
		//アカウント状態の変更
		UserDao.changeUserCondition(userId);
	}

	public static User findUser(String userId) {
		//編集するユーザー情報の取得
		return UserDao.findUser(userId);
	}

	public static Map<Integer, String> getAllbranches(){
		return UserDao.getAllbranches();
	}

	public static Map<Integer, String> getAllPositions(){
		return UserDao.getAllPositions();
	}
}
