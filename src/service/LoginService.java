package service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import beans.LoginUser;
import dao.UserDao;
import utils.CipherUtil;

public class LoginService {
	public static LoginUser login (HttpServletRequest request) {
        String id = request.getParameter("id");
        String password = CipherUtil.encrypt(request.getParameter("password"));

        return UserDao.checkUser(id, password);
	}

	public static void logout (HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.invalidate();
	}
}
