package service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import beans.LoginUser;
import beans.UserMessage;
import dao.CommentDao;
import dao.PostDao;


public class PostService {
	public static List<UserMessage> getAllPost(){
		return PostDao.getAllPost();
	}

	public static void addPost(HttpServletRequest request, LoginUser loginUser) {
		PostDao.insert(request, loginUser);
	}

	public static void deletePost(HttpServletRequest request) {
		PostDao.delete(request.getParameter("postId"));
		CommentDao.deleteByPostId(request.getParameter("postId"));
	}

	public static List<UserMessage> getPostsByCategory(String word){
		if(word.isEmpty()) {
			return new ArrayList<UserMessage>();
		} else {
			return PostDao.searchPostByCategory(word);
		}
	}

	public static List<UserMessage> getPostsByDate(String startDate, String endDate){
		if(startDate.isEmpty() || endDate.isEmpty()) {
			return new ArrayList<UserMessage>();
		} else {
			return PostDao.searchPostsByDate(startDate, endDate);
		}
	}
}
