package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.LoginUser;
import service.PostService;
import utils.ValidateContent;


/**
 * Servlet implementation class AddPostServlet
 */
@WebServlet("/AddPost")
public class AddPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddPostServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/new_post/post.jsp").forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 List<String> validationMessages = ValidateContent.validatePost(request);

		if (validationMessages.isEmpty()) {
			PostService.addPost(request, (LoginUser)request.getSession().getAttribute("loginUser"));
			response.sendRedirect("Top");
        } else {
        	request.getSession().setAttribute("validationMessages", validationMessages);
        	response.sendRedirect("AddPost");
        }
	}

}
