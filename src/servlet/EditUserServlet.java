package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserService;
/**
 * Servlet implementation class EditUserServlet
 */
@WebServlet("/EditUser")
public class EditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		User user = UserService.findUser(request.getParameter("userId"));
		request.setAttribute("user", user);
		request.setAttribute("branchMap", UserService.getAllbranches());
		request.setAttribute("positionMap", UserService.getAllPositions());
		request.getRequestDispatcher("/user_management/edit_user.jsp").forward(request, response);
	}

}
