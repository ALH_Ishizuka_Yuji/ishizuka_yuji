package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserMessage;
import service.PostService;

/**
 * Servlet implementation class SearchByCategoryServlet
 */
@WebServlet("/SearchByCategory")
public class SearchByCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchByCategoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<UserMessage> postByCategory = PostService.getPostsByCategory(request.getParameter("categoryName"));
		if( postByCategory.isEmpty()) {
			postByCategory = PostService.getAllPost();
			request.getSession().setAttribute("validationMessages", "該当するカテゴリーの投稿はありませんでした");
		}
		request.setAttribute("allPosts", postByCategory);
		request.getRequestDispatcher("Top").forward(request, response);
	}

}
