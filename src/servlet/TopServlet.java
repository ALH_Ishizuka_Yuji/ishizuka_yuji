package servlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.PostService;

/**
 * Servlet implementation class TopServlet
 */
@WebServlet("/Top")
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TopServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	if(request.getAttribute("allPosts") == null) {
    		List<UserMessage> allPosts = PostService.getAllPost();
    		 request.setAttribute("allPosts", allPosts);
    	}
    	if(request.getAttribute("allComments") == null) {
    		Map<Integer, List<UserComment>> allComments = CommentService.getAllComments();
            request.setAttribute("allComments", allComments);
    	}
		request.getRequestDispatcher("home/home.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getAttribute("allPosts") == null) {
   		List<UserMessage> allPosts = PostService.getAllPost();
   		request.setAttribute("allPosts", allPosts);
		}
		if(request.getAttribute("allComments") == null) {
    		Map<Integer, List<UserComment>> allComments = CommentService.getAllComments();
            request.setAttribute("allComments", allComments);
    	}
		request.getRequestDispatcher("home/home.jsp").forward(request, response);
	}

}
